var http = require('http'),
    httpProxy = require('http-proxy'); // load the proxy and http libraries

var projects = {}; // keep track of our projects

var railsHost = "localhost"; // info about the rails host
var railsPort = 3001;
var railsAddr = "http://" + railsHost + ":" + railsPort;

var codeboxRegex = /^\/codebox\/(\d+)(\/.*)$/; // regex to match CodeBox requests

var apiKey = "1Bvcs1Weyvf7NFWr7fE4"; // api key for the rails app. Change this on your own instance, or else!
var apiEndpoint = "/proxy-api/"; // the API endpoint that we should host
var apiEndpoint_setproject = "setproject"; // the command to set the project

// ask for a update on the running containers
function requestUpdate() {
	var http = require("http");
	var options = {
		hostname: railsHost,
		port: railsPort,
		path: '/api/refresh/' + apiKey
	};
	var req = http.request(options, function(res) {
		res.on('data', function (body) {
			// don't care what we get back
		});
	});
	// write data to request body
	req.write('Anyone home?');
	req.end();
}

requestUpdate(); // update first, so we start with a accurate list

// create the proxy server
var proxy = httpProxy.createProxyServer({});

// start a HTTP server to accept connections
var server = http.createServer(function(req, res) {
	// respond to a HTTP query
	
	// find the requested URL
	var url = req.url;
	
	// if it from the API, it should be a post request
	if(url.startsWith(apiEndpoint)) {
		var body = ''; // the body of the post request
		req.on('data', function (data) {
			body += data; // when we get another piece of the contents
		});
		req.on('end', function () {
			apiCallDone(url, body); // the request is done. Forward it on.
		});
		res.writeHead(200, {
			'Content-Type': 'text/plain' // 200 = request ok
		});

		res.end('Query recieved.'); // don't need to give out any more info.
		return;
	}
	
	// match it to that of a codebox URL
	var codeboxMatch = codeboxRegex.exec(url);
	
	// if it is on a codebox, respond
	if(codeboxMatch != null) {
		var projectID = codeboxMatch[1]; // the project id
		var codeboxUrl = codeboxMatch[2]; // the URL inside codebox
		var box = projects[projectID]; // the project that is is targeting
		if(box != null) {
			var target = box.address + codeboxUrl; // find the target address.
			proxy.web(req, res, { target: target, ignorePath: true });
		} else {
			res.writeHead(503, {
				'Content-Type': 'text/plain'
			});

			res.end('This box is not running.');
		}
	} else { // otherwise forward it on to rails
		var target = railsAddr + url;
		proxy.web(req, res, { target: target });
	}
});

// when someone calls our API with data.
function apiCallDone(url, data) {
	url = url.substring(apiEndpoint.length); // find only the last part of the url.
	if(url.startsWith(apiEndpoint_setproject)) { // if it the update projects endpoint
		try { // incase the json is bad. Stop people from DoS-ing us
			var doc = JSON.parse(unescape(data)); // parse the json
			if(doc.api_key == apiKey) { // if they got the api key corrent
				projects = {}; // reset the projects
				for(var id in doc.projects) { // for each project
					var data = doc.projects[id]; // find the data from it
					projects[id] = data; // put that in as a project
				}
// 				console.log(projects); // log out the projects.
			}
		} catch(err) {
			// boo hoo hoo.
		}
	}
}

console.log("listening on port 5050");
server.listen(5050);
